import datetime
from parse2json import *

def parseMain(site_name):
    now = datetime.datetime.now()

    storeCommentUrls = site_name + '-comment-urls.txt'
    REVIEW_PATH = './data/' + str(now.year) + '/' + str(now.month) + '/' + str(now.day - 1)  + '/' + storeCommentUrls

    storeCommentCSV = site_name + '-comment-urls.csv'
    STORE_PATH = './data/' + str(now.year) + '/' + str(now.month) + '/' + str(now.day - 1)  + '/' + storeCommentCSV

    reviewUrl = readReviewUrl(REVIEW_PATH)
    
    dataList = filterData(reviewUrl, site_name)

    writeCSV(STORE_PATH,dataList)

    print("TOTAL COMMENTS : " + str(len(dataList)))

# if __name__ == "__main__":
#     parseMain()