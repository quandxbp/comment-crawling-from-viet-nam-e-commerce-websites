import urllib
import json
import unicodecsv as csv

def readReviewUrl(reviewPath):
    reviewUrl = list()
    with open(reviewPath, 'r') as file:
        for jsonUrl in file:
            jsonUrl = jsonUrl.replace('\n', '')
            reviewUrl.append(jsonUrl)
    return reviewUrl

def filterData(reviewUrlList,site_name):
    headers = {'User-Agent': 'User-Agent:Mozilla/5.0'}
    dataList = list()

    for url in reviewUrlList:
        print(url)
        try:
            # Fake Mozilla header to get permission
            dataTemp = urllib.request.Request(url, headers=headers)
            res = urllib.request.urlopen(dataTemp).read()
            obj = json.loads(res)
            
            if site_name is 'shoppee':
                data = obj['data']['ratings']
                
                for review in data:
                    content = review['comment']
                    purchased = ""
                    rating = review['rating_star']

                    # Get tag review
                    tagComments = review['tags']
                    if not tagComments is None:
                        for tag in tagComments:
                            content = content + " " + tag['tag_description']

                    # If no content -> Next
                    if content == "": continue

                    dataList.append((content, purchased, rating))
            elif site_name is 'tiki':
                # Check number of reviews, if = 0 => continue
                if obj["reviews_count"] == 0 :
                    print("No comment => next")
                    continue
                lastPage = obj["paging"]["last_page"]

                for index in range(1, int(lastPage) + 1):
                    requestCommentUrl = url + "&page=" + str(index)
                    # Fake Mozilla header to get permission
                    try:
                        dataTemp = urllib.request.Request(requestCommentUrl, headers=headers)
                        res = urllib.request.urlopen(dataTemp).read()
                        obj2 = json.loads(res)
                        print("Crawling - " + requestCommentUrl)
                        # data is a list of reviews/ comments
                        data = obj2["data"]
                        for review in data:
                            content = review["title"] + " " + review["content"]
                            purchased = review["created_by"]["purchased"]
                            rating = review["rating"]
                            dataList.append((content,purchased,rating))
                    except:
                        print("Failed: " + requestCommentUrl)
            elif site_name is 'sendo':
                if obj["result"]["meta_data"]["total_count"] == 0:
                    print("No comment => next")
                    continue
                lastPage = obj["result"]["meta_data"]["total_page"]

                for index in range(1, int(lastPage) + 1):
                    requestCommentUrl = url.replace("p=1", "p=" + str(index))
                    try:
                        dataTemp = urllib.request.Request(requestCommentUrl, headers=headers)
                        res = urllib.request.urlopen(dataTemp).read()
                        obj2 = json.loads(res)
                        print("Crawling - " + requestCommentUrl)
                        # data is a list of reviews/ comments
                        data = obj2["result"]["data"]
                        for review in data:
                            content = review["content"]
                            # If the content is null
                            if content == "": continue
                                
                            purchased = "false"
                            rating = review["star"]
                            dataList.append((content,purchased,rating))
                    except:
                        print("Failed: " + requestCommentUrl)
        except:
            print("Failed: " + url)

    #return data list
    return dataList

def writeCSV(storePath ,dataList):
    with open(storePath, 'wb') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(["Content","isPurchased","Rating"])
        
        # VAR count to calculate the remaining element in list
        count = 0
        for group in dataList:
            content = group[0]
            purchased = str(group[1])
            rating = group[2]

            if rating in [1,2,3]:
                rate = "negative"
            elif rating in [5]:
                rate = "positive"
            else:
                rate = "neutral"
            row = [content, purchased, rate]
            writer.writerow(row)

            # VAR remainElement to check the remaining element
            remainElement = len(dataList) - count
            print("Remaining element : " + str(remainElement))
            # increase count to 1
            count += 1


