import sys, os
import datetime
import shutil


sys.path.append('./data/')

NOW = datetime.datetime.now()

DATA_DIR = './data/' + str(NOW.year) + '/' + str(NOW.month) + '/'


def check_miss_day():
    for day in range(1, NOW.day + 1):
        if not os.path.exists(DATA_DIR + str(day)):
            print("Not exist data : " + DATA_DIR + str(day))


def create_miss_day(miss_day):
    if os.path.exists(DATA_DIR + str(miss_day)):
        print(DATA_DIR + str(miss_day) + ' is already existed !')
    else:
        detected_day = None
        # Check the existed day bofore the miss day
        for find_day in range(1, int(miss_day) + 1):
            if os.path.exists(DATA_DIR + str(find_day)):
                detected_day = str(find_day)

        # If there is no before crawl day then get the after crawl day
        if detected_day is None:
            for find_day in range(int(miss_day) + 1, + NOW.day + 1):
                if os.path.exists(DATA_DIR + str(find_day)):
                    detected_day = str(find_day)
                    break

        # If still no day after the miss day then print out
        if detected_day is None:
            print("Crawl today data first ...")
            return
        # else copy the exist day to the miss day
        else:
            # Create the mising day first
            destination = DATA_DIR + str(miss_day)
            print("Copying from " + DATA_DIR + detected_day + ' to ' + destination)
            shutil.copytree(DATA_DIR + detected_day, destination)
            print("Finish copying ...")


def auto_generate():
    miss_day = []
    for day in range(1, NOW.day + 1):
        if not os.path.exists(DATA_DIR + str(day)):
            miss_day.append(day)
    list(map(lambda x: create_miss_day(x), miss_day[::-1]))
    return miss_day


def remove_raw_folder():
    directory = DATA_DIR + str(NOW.day) + "/raw"
    if os.path.isdir(directory):
        print("Removing " + directory)
        shutil.rmtree(directory, ignore_errors=True)
    else:
        print(directory + " is not existed ?")


if __name__ == "__main__":
    check_miss_day()
