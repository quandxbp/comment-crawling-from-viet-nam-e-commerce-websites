from link_finder import LinkFinder
from general import *
import datetime

now = datetime.datetime.now()

WRITE_DIR = './data/' + str(now.year) + '/' + str(now.month) + '/' + str(now.day) + '/'

class Spider:

    project_name = ''

    def __init__(self, site_name):
        Spider.project_name = WRITE_DIR
        self.boot(site_name)
        self.gather_links(site_name)

    @staticmethod
    def boot(site_name):
        # create the directory
        create_project_dir(Spider.project_name)

    @staticmethod
    def gather_links(site_name):
        Finder = LinkFinder()
        if site_name == 'tiki':
            for url in Tiki_URLS:
                Finder.getProductCommentTiki(url)
        elif site_name == 'shoppee':
            for url in Shoppee_URLS:
                Finder.getCommentShoppee(url)
        elif site_name == 'sendo' :
            for url in Sendo_URLS:
                Finder.getCommentSendo(url)
        
        # Get all review links
        links = Finder.page_links()

        # Create a exporting all urls and comment urls text file
        storeProductUrlFile = site_name + '-product-urls.txt'
        productUrlPath = Spider.project_name + '/' + storeProductUrlFile
        storeCommentUrlFile = site_name + '-comment-urls.txt'
        commentUrlPath = Spider.project_name + '/' + storeCommentUrlFile

        for url in links:
            print("Extracting - " + url)
            # Create all_urls.txt
            append_to_file(productUrlPath, url)
        
            # Ex :Get "p1587275.html" in "https://tiki.vn/ipad-mini-4-128gb-wifi-3g-4g-vang-hang-chinh-hang-p1587275.html"
            if site_name == 'tiki':
                productId = url.split('-')[-1]
                # Ex :Get "1587275" in "p1587275.html"
                productId = productId.split('.')[0].replace("p","")
                # Create comments-urls.txt
                commentUrl = "https://tiki.vn/api/v2/reviews?product_id=" + productId
            elif site_name == 'shoppee':
                commentUrl = url
            elif site_name == 'sendo' :
                commentUrl = url
        
            append_to_file(commentUrlPath, commentUrl)
        print("Total link in " + site_name + " : " + str(len(links)))

        # filter the duplicate product urls
        set_to_file(file_to_set(productUrlPath), productUrlPath)
        set_to_file(file_to_set(commentUrlPath), commentUrlPath)


