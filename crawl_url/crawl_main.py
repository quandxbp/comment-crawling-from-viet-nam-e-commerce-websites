from spider import *
from general import *
import shutil


def crawlMain(site_name):
    # This line is to remove folder before recrawl
    shutil.rmtree(WRITE_DIR, ignore_errors=True)

    if site_name == 'tiki':
        tikiSpider = Spider("tiki")
    elif site_name == 'shoppee':
        shoppeeSpider = Spider("shoppee")
    elif site_name == 'sendo':
        sendoSpider = Spider("sendo")

    print("Program Finish !")

