from bs4 import BeautifulSoup
from urllib.request import urlopen
from general import *
import urllib
import json


class LinkFinder():
    def __init__(self):
        self.total_links = 0
        self.links = list()

    def page_links(self):
        return self.links

    def error(self, message):
        pass

    def fakeUser(self, url):
        headers = {'User-Agent': 'User-Agent:Mozilla/5.0'}
        try:
            dataTemp = urllib.request.Request(url, headers=headers)
            data = urllib.request.urlopen(dataTemp).read()
            soup = BeautifulSoup(data, "lxml")
        except Exception:
            print('Exception : ' + str(Exception.__class__.__name__))

        return soup

    def getProductCommentTiki(self, url):
        soup = self.fakeUser(url)

        h4Result = soup.find("h4", {"name": "results-count"})
        totalProduct = h4Result.get_text().split(" ")[1]

        # 35 is 35 product in a page
        lastPage = (int(totalProduct) / 47) + 1

        # If > 200 => lastPage = 200
        if int(lastPage) > 200:
            lastPage = 200

        for index in range(1, int(lastPage) + 1):
            crawlUrl = url + "?src=mega-menu&page=" + str(index)
            print('Crawling - ' + crawlUrl)
            soup2 = self.fakeUser(crawlUrl)
            try:
                division = soup2.find("div", {"class": "product-box-list"})
                anchors = division.find_all('a')
                for anchor in anchors:
                    href = anchor.get('href')
                    if '?' in href:
                        href = href.split('?')[:-1][0]
                    if '.html' not in href:
                        continue
                    self.links.append(href)
                    self.total_links += 1
            except Exception:
                print("Fail : - " + crawlUrl)
                continue

    def getCommentShoppee(self, url):
        newest = 0
        
        while True:
            repNewest = "newest=" + str(newest)
            crawlUrl = url.replace("newest=0", repNewest)
            
            print(crawlUrl)
            
            try:
                res = urllib.request.urlopen(crawlUrl).read()
                obj = json.loads(res)

                # If list is empty => break the loop
                # print(len(obj["items"]))
                if (len(obj["items"])) is 0:
                    print("Empty list -> break")
                    break

                data = obj["items"]
                for product in data:
                    itemId = product['itemid']
                    shopId = product['shopid']

                    reviewUrl = """https://shopee.vn/api/v2/item/get_ratings?filter=0&flag=1&itemid=""" + str(itemId) + """&limit=100&offset=0&shopid=""" + str(shopId) + """&type=0"""

                    self.links.append(reviewUrl)
                    self.total_links += 1
                newest += 50
            except Exception:
                print("Fail : - " + crawlUrl)
                newest += 50
                continue
    
    def getCommentSendo(self, url):
        # url = 'https://www.sendo.vn/m/wap_v2/category/product?category_id=2901&p=1&s=100'
        page = 1

        while True:
            repPage = 'p=' + str(page)
            crawlUrl = url.replace("p=1", repPage)

            print("Crawling : " + str(crawlUrl))

            try: 
                res = urllib.request.urlopen(crawlUrl).read()
                obj = json.loads(res)

                # If list is empty => break the loop
                if (len(obj["result"]['data'])) is 0:
                    print("Empty list -> break")
                    break
                
                data = obj["result"]['data']
                
                for product in data:
                    # If total rate is equal to 0 => Next
                    totalRate = product['rating_info']['total_rated']
                    if totalRate == 0:
                        continue
                    # Get product ID
                    productId = product['id']
                    reviewUrl = "https://www.sendo.vn/m/wap_v2/san-pham/rating/" + str(productId) + "?p=1&s=100"
                    self.links.append(reviewUrl)
                    self.total_links += 1

                # increase number of page
                page += 1
            except Exception:
                print("Fail : - " + crawlUrl)
                page += 1
                continue
        
    # def getCommentVatGia(self, url):


            

