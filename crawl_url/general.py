import os
import json, codecs

# Each website which is crawled is a separated project (folder)
def create_project_dir(directory):
    if os.path.exists(directory):
        print(directory + ' is already created !')
    else:
        print('Create directory : ' + directory)
        os.makedirs(directory)


def create_brand_dir(project_name, brand_name):
    print('Create brand directory ...')
    brand_dir = project_name + '/' + brand_name;
    if os.path.exists(brand_dir):
        print(brand_name + ' is already created !')
    else:
        print('Create brand directory ...')
        os.makedirs(brand_dir)


def create_data_files(project_name, site_name):
    path = project_name + '/' + site_name
    textfile = path + '/' + site_name + '_phone-urls.txt'
    create_project_dir(path)
    if not os.path.isfile(textfile):
        write_file(textfile, '')


def append_urls_to_file(project_name, site_name, links, brand_list):
    for brand_dir in brand_list:
        path = project_name + '/' + brand_dir + '/' + site_name + '/' + site_name + '_phone-urls.txt'
        for brand, url in links.items():
            if brand[0] == brand_dir:
                append_to_file(path, url)
        # Make list of urls in file become to a set and then return to file

        urls = file_to_set(path)
        set_to_file(urls, path)


def write_file(path, data):
    f = open(path, 'w+')
    f.write(data)
    f.close()


def append_to_file(path, data):
    if not os.path.isfile(path):
        f = open(path, 'w+')
        f.write('')
    with open(path, 'a') as file:
        file.write(data + '\n')


def delete_file_contents(path):
    with open(path, 'w'):
        pass


# Read a file then convert each line to set items
def file_to_set(file_name):
    results = set()
    with open(file_name, 'rt') as f:
        for line in f:
            results.add(line.replace('\n', ''))
    return sorted(results)


# Iterate through a set, each item will be a new line in the file
def set_to_file(links, file):
    delete_file_contents(file)
    for link in links:
        append_to_file(file, link)

Tiki_URLS = ['https://tiki.vn/dien-tu-dien-lanh/c4221',
        'https://tiki.vn/laptop-may-vi-tinh/c1846',
        'https://tiki.vn/thiet-bi-kts-phu-kien-so/c1815',
        'https://tiki.vn/may-anh/c1801',
        'https://tiki.vn/dien-gia-dung/c1882',
        'https://tiki.vn/nha-cua-doi-song/c1883',
        'https://tiki.vn/bach-hoa-online/c4384',
        'https://tiki.vn/do-choi-me-be/c2549',
        'https://tiki.vn/lam-dep-suc-khoe/c1520',
        'https://tiki.vn/thoi-trang/c914',
        'https://tiki.vn/the-thao-da-ngoai/c1975',
        'https://tiki.vn/o-to-xe-may-xe-dap/c8594',
        ]
Shoppee_URLS = ['https://shopee.vn/api/v2/search_items/?by=pop&limit=50&match_id=84&newest=0&order=desc&page_type=search',
        'https://shopee.vn/api/v2/search_items/?by=pop&limit=50&match_id=13033&newest=0&order=desc&page_type=search',
        'https://shopee.vn/api/v2/search_items/?by=pop&limit=50&match_id=2365&newest=0&order=desc&page_type=search',
        'https://shopee.vn/api/v2/search_items/?by=pop&limit=50&match_id=13030&newest=0&order=desc&page_type=search',
        'https://shopee.vn/api/v2/search_items/?by=pop&limit=50&match_id=2353&newest=0&order=desc&page_type=search'
        ]

Sendo_URLS = ['https://www.sendo.vn/m/wap_v2/category/product?category_id=2901&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=8&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=94&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=8&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1686&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1722&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1366&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=220&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1663&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=2901&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=528&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=2101&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1366&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=3229&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1954&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1108&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=2075&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=1019&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=528&p=1&s=100',
        'https://www.sendo.vn/m/wap_v2/category/product?category_id=3228&p=1&s=100'
        ]