import sys
sys.path.append('./crawl_url/')
sys.path.append('./parse-json-data/')

from crawl_main import crawlMain
from parse_main import parseMain
from utils import *


def main(argv):
    if (len(argv) < 1) or (argv[0] == 'menu'):
        print_menu()
    elif argv[0] == 'run':
        if len(argv) < 2:
            print_run_usage()
        elif argv[1] == 'crawl':
            if len(argv) < 3:
                print_crawl_usage()
            elif argv[2] == 'tiki':
                crawlMain('tiki')
            elif argv[2] == 'shoppee':
                crawlMain('shoppee')
            elif argv[2] == 'sendo':
                crawlMain('sendo')
            else:
                print_crawl_usage()
        elif argv[1] == 'parser':
            if len(argv) < 3:
                print_parse_usage()
            elif argv[2] == 'tiki':
                parseMain('tiki')
            elif argv[2] == 'shoppee':
                parseMain('shoppee')
            elif argv[2] == 'sendo':
                parseMain('sendo')
            else:
                print_parse_usage()
        else:
            print_run_usage()
    else:
        print_menu()


def print_menu():
    print("-----------------------------------")
    print("|--------- RUN ONE BY ONE ---------")
    print("|--> python run.py run ")

def print_run_usage():
    print("|--> python run.py run crawl")
    print("|--> python run.py run parser")

def print_crawl_usage():
    print("|--> python run.py run crawl tiki")
    print("|--> python run.py run crawl shoppee")
    print("|--> python run.py run crawl sendo")

def print_parse_usage():
    print("|--> python run.py run parser tiki")
    print("|--> python run.py run parser shoppee")
    print("|--> python run.py run parser sendo")

if __name__ == "__main__":
    main(sys.argv[1:])
